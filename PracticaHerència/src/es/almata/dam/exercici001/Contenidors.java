package es.almata.dam.exercici001;

public abstract class Contenidors {
	private String nom;
	private double capacitat;
	private String contingut;

	
	public abstract void omplir();
	public abstract void buidar();
	
	public Contenidors() {
		super();
	}
	
	public Contenidors(String nom, double capacitat, String contingut) {
		super();
		this.nom = nom;
		this.capacitat = capacitat;
		this.contingut = contingut;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public double getCapacitat() {
		return capacitat;
	}
	public void setCapacitat(double capacitat) {
		this.capacitat = capacitat;
	}
	public String getContingut() {
		return contingut;
	}
	public void setContingut(String contingut) {
		this.contingut = contingut;
	}
	
}
