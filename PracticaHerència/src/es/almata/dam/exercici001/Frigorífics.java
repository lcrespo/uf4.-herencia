package es.almata.dam.exercici001;

public class Frigorífics extends Contenidors {
	private double temperatura;
	
	public Frigorífics() {
		super();
	}
	
	public Frigorífics(double temperatura) {
		super();
		this.temperatura = temperatura;
	}

	public double getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(double temperatura) {
		this.temperatura = temperatura;
	}
	
	@Override
	public void omplir() {
		// TODO Auto-generated method stub

	}

	@Override
	public void buidar() {
		// TODO Auto-generated method stub

	}

	

}
