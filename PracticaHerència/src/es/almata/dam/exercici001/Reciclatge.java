package es.almata.dam.exercici001;

public class Reciclatge extends Contenidors {
	private String color;
	
	public Reciclatge() {
		super();
	}
	
	public Reciclatge(String color) {
		super();
		this.color = color;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public void omplir() {
		

	}

	@Override
	public void buidar() {
		// TODO Auto-generated method stub

	}

}
